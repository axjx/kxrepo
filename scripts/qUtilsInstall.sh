echo "Setting Up QUTILS"

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "The currenct working directory is: `pwd`"
echo "Script location directory is identified as : $DIR"

ROOTDIR=`dirname $DIR`
echo "REPOSITORY root is identified as : $ROOTDIR"


##  create a dir for qutil
QUTILDIR=$ROOTDIR/qutilshome
mkdir -p $QUTILDIR

###  git cloning --  https://github.com/nugend/qutil  --- 

git clone https://github.com/nugend/qutil.git  $QUTILDIR 


##  create QPATH and links 
##  linkes t
export QPATH=$ROOTDIR/qpath
mkdir -p $QPATH

ln -s  $QUTILDIR/lib $QPATH/qutil
ln -s  $QUTILDIR/lib/bootstrap.q $QPATH/bootstrap.q


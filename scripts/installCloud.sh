## automated scripts to install cloud

### 32 bit arch for kdb 
sudo dpkg --add-architecture i386
sudo apt-get update
sudo apt-get install -y libc6:i386 libstdc++6:i386

## for KDB 
sudo apt-get install -y rlwrap  # to wrap line

# set kdb env 
. kxrepo/scripts/env36.sh

## install R
##   see here https://cran.r-project.org/bin/linux/ubuntu/README.html
##   and here  https://pages.github.nceas.ucsb.edu/NCEAS/help/installing_r_on_ubuntu.html
##    - this is how to automate repo addition  (otherwise R may be old )
##    http://manpages.ubuntu.com/manpages/bionic/man1/add-apt-repository.1.html 
## MAIN ONE IS THIS : https://www.digitalocean.com/community/tutorials/how-to-install-r-on-ubuntu-18-04 

sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
sudo add-apt-repository 'deb https://cloud.r-project.org/bin/linux/ubuntu bionic-cran35/'
sudo apt-get update
sudo apt-get install -y r-base r-base-dev

sudo apt-get install gdebi-core

wget https://download2.rstudio.org/server/trusty/amd64/rstudio-server-1.2.1335-amd64.deb
sudo gdebi rstudio-server-1.2.1335-amd64.deb

#wget https://download2.rstudio.org/rstudio-server-1.1.463-amd64.deb
#sudo gdebi rstudio-server-1.1.463-amd64.deb
#sudo dpkg -i rstudio-server-1.1.463-amd64.deb # see heer 

## for devtools 
sudo apt-get -y install libcurl4-gnutls-dev libcairo2-dev libxt-dev  libssl-dev libssh2-1-dev
sudo apt-get -y install libxml2-dev  # for rvest 

sudo su - -c "R -e \"install.packages('shiny', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('rmarkdown', repos='http://cran.rstudio.com/')\""

sudo su - -c "R -e \"install.packages('DT', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('zoo', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('xts', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('dygraphs', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('htmlwedgets', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('htmltools', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('ggplot2', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('gridExtra', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('ggthemes', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('shinyBS', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('data.table', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('reshape2', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('dplyr', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('shinyjs', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('magrittr', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('scales', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('d3r', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('scatterD3', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('shinydashboard', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('ggbeeswarm', repos='http://cran.rstudio.com/')\""


sudo su - -c "R -e \"install.packages('tidyverse', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('tidyquant', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('tsoutliers', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('quantmod', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('PerformanceAnalytics', repos='http://cran.rstudio.com/')\""
 

sudo su - -c "R -e \"install.packages('devtools', repos='http://cran.rstudio.com/')\""

sudo su - -c "R -e \"devtools::install_github('rstudio/dygraphs')\""
sudo su - -c "R -e \"devtools::install_github('timelyportfolio/d3hierR')\""


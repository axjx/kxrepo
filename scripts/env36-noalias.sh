
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "The currenct working directory is: `pwd`"
echo "Script location directory is identified as : $DIR"

ROOTDIR=`dirname $DIR`
echo "REPOSITORY root is identified as : $ROOTDIR"

## kdb version to be used 
KDBVERSION=l32/3.6/2018.05.17

# this is for qcon, should be same across all
alias qcon=$ROOTDIR/kx/qcon


export QHOME=$ROOTDIR/kx/$KDBVERSION

## make sure it is not run many times
## as it adds a new directory each time  
export PATH=$QHOME/l32:$PATH


### from https://code.kx.com/wiki/Cookbook/SSL
## By default kdb+ will try to verify the server's certificate against a trusted source
## using the certificates from SSL_CA_CERT_FILE or SSL_CA_CERT_PATH to verify the server's certificate
## If you don't wish to verify a server's certificate, set
export SSL_VERIFY_SERVER=NO

export LD_LIBRARY_PATH=$QHOME/l32

# aliases for scripts to run q32 and q64 in parallel
# alias q="q32"

# this is simple alias to get ip of the server 
alias myip="curl http://ipecho.net/plain;echo"

# home of the main dir
export BT_LIBHOME=$ROOTDIR

# kdb databases root location
export BT_KDBDATA=/var/kdb/db

# kdb raw files for download
export BT_RAWDATA=/var/kdb/raw
